import com.sparetimecoders.GitlabPublisherPlugin

lazy val alpakkaVersion = "0.20"
lazy val akkaVersion = "2.5.18"
lazy val typesafeConfigVersion = "1.3.3"
lazy val logbackVersion = "1.2.3"
lazy val logbackJsonVersion = "4.11"
lazy val scalaTestVersion = "3.0.5"
lazy val scalaLoggingVersion = "3.9.0"
lazy val json4sVersion = "3.6.2"

lazy val `rabbitmq-library` = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "sparetimecoders",
      scalaVersion := "2.12.6"
    )),
    name := "rabbitmq-library",
    releaseIgnoreUntrackedFiles := true,
    publishTo := Some("Gitlab" at "https://gitlab.com/api/v4/projects/9504979/packages/maven"),
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
      "com.typesafe" % "config" % typesafeConfigVersion,
      "com.lightbend.akka" %% "akka-stream-alpakka-amqp" % alpakkaVersion,
      "ch.qos.logback" % "logback-classic" % logbackVersion,
      "com.typesafe.scala-logging" %% "scala-logging" % scalaLoggingVersion,
      "org.json4s" %% "json4s-jackson" % json4sVersion,
      "org.json4s" %% "json4s-ext" % json4sVersion,

      "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion % Test,
      "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test,
      "org.scalatest" %% "scalatest" % scalaTestVersion % Test
    )
  )

enablePlugins(JavaAppPackaging, AshScriptPlugin, GitlabPublisherPlugin)
