FROM pliljenberg/scala-sbt:2.12.6-1.1.6 as builder
WORKDIR /build

COPY . /build
ARG BUILD_GOALS="clean coverage test coverageReport coverageOff stage"

RUN sbt ${BUILD_GOALS}
