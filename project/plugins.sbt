resolvers += "Gitlab" at "https://gitlab.com/api/v4/groups/sparetimecoders/-/packages/maven"
//resolvers += "Gitlab" at "https://gitlab.com/api/v4/packages/maven"


addSbtPlugin("org.scalariform" % "sbt-scalariform" % "1.8.2")
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.3.20")
addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.5.1")
addSbtPlugin("com.github.gseitz" % "sbt-release" % "1.0.10")
addSbtPlugin("sparetimecoders" % "sbt-gitlabpublisher" % "0.1.1")
