package stc.rabbitmq

import akka.actor.ActorSystem
import akka.stream.alpakka.amqp.scaladsl.CommittableIncomingMessage
import akka.stream.scaladsl.{Sink, Source}
import akka.stream.testkit.scaladsl.TestSink
import akka.stream.{KillSwitches, SharedKillSwitch}
import akka.testkit.TestKit
import org.json4s.jackson.Serialization.write
import org.scalatest.Matchers
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.time.{Millis, Span}
import stc.rabbitmq.FlowBuilder.DefaultFlowBuilder

import scala.collection.mutable.ListBuffer
import scala.concurrent.Future

class FlowbuilderSpec extends TestKit(ActorSystem("FlowbuilderSpec"))
  with Matchers
  with ScalaFutures
  with TestBase
  with TableDrivenPropertyChecks {
  implicit val config: PatienceConfig = PatienceConfig(timeout = scaled(Span(1500, Millis)))
  val sharedKillSwitch: SharedKillSwitch = KillSwitches.shared("")

  "Flowbuilder" should "store routingkeys" in {
    val builder = FlowBuilder(1)
      .add[String, String]("routingKey", _ => Future("result"))
    builder.routingKeys should have size 1
    builder.routingKeys should contain("routingKey")
  }

  it should "build a flow that diverts to multiple flows" in {

    val messages = new ListBuffer[String]()
    val events = new ListBuffer[String]()

    val builder = FlowBuilder(1)
      .add[Message, String]("message", messageHandler, watchableSink(messages))
      .add[Event, String]("event", eventHandler, watchableSink(events))
    builder.routingKeys should have size 2

    val message = incomingMessage(write(Message("message", None)), "message")
    val event = incomingMessage(write(Event("event", None)), "event")

    Source.fromIterator(() => Seq(message, event).iterator)
      .via(builder.build(sharedKillSwitch))
      .runWith(TestSink.probe[CommittableIncomingMessage])
      .request(2)
      .expectComplete()

    Thread.sleep(250) //Horror...
    messages should have size 1
    events should have size 1
  }

  it should "be able to use wildcard bindings" in {

    val messages = new ListBuffer[String]()

    val builder = FlowBuilder(1)
      .add[Message, String]("message.#", messageHandler, watchableSink(messages))
    builder.routingKeys should have size 1

    val message = incomingMessage(write(Message("message", None)), "message.test")

    Source.fromIterator(() => Seq(message).iterator)
      .via(builder.build(sharedKillSwitch))
      .runWith(TestSink.probe[CommittableIncomingMessage])
      .request(1)
      .expectComplete()

    Thread.sleep(250) //Horror...
    messages should have size 1
  }

  it should "acknowledge message if json is invalid for flow" in {

    val builder = FlowBuilder(1)
      .add[Event, String]("event", eventHandler)

    val messageSentToEvent = incomingMessage(write(Message("message", None)), "event")

    Source(List(messageSentToEvent))
      .via(builder.build(sharedKillSwitch))
      .runWith(TestSink.probe[CommittableIncomingMessage])
      .request(1)
      .expectComplete()

    messageSentToEvent.acked should be(true)
    messageSentToEvent.nacked should be(false)
  }

  it should "acknowledge unhandled message" in {
    val builder = FlowBuilder(1)
      .add[Event, String]("event", eventHandler)

    val message = incomingMessage(write(Message("message", None)), "message")

    val unahandled = Source(List(message))
      .via(builder.build(sharedKillSwitch))
      .runWith(Sink.seq)
      .futureValue

    unahandled should have size 1
    message.acked should be(true)
    message.nacked should be(false)
  }

  it should "throw exception when trying to add duplicate routingkeys" in {
    val builder = FlowBuilder(1)
      .add[Event, String]("event", eventHandler)

    the[IllegalStateException] thrownBy {
      builder.addRequestResponse[String, String]("event", s => Future(s))
    } should have message "Routingkey [event] already exists in Flowbuilder."

    the[IllegalStateException] thrownBy {
      builder.add[String, String]("event", s => Future(s))
    } should have message "Routingkey [event] already exists in Flowbuilder."
  }

  it should "throw exception when adding a null function" in {
    the[IllegalStateException] thrownBy {
      FlowBuilder(1)
        .add[Event, String]("event", null)
    } should have message s"Trying to add a null function for routingkey [event]"
  }

  it should "match binding pattern" in {
    val builder = new DefaultFlowBuilder(1)
    val matching = Table(
      ("incoming", "wanted"),
      ("Event.Error", "Event.#"),
      ("Event.test.more", "Event.#"),
      ("Error.Event", "Error.Event"),
      ("Error.Event.other", "*.Event.*"),
      ("EVENT.Error", "EVENT.Error")
    )

    forAll(matching) { (incoming: String, wanted: String) =>
      builder.compareRoutingKeys(incoming, wanted) should be(true)
    }
  }

  it should "not match binding pattern" in {
    val builder = new DefaultFlowBuilder(1)
    val notMatching = Table(
      ("incoming", "wanted"),
      ("Error.other", "Event.#"),
      ("Error.Event.other", "Event.#"),
      ("Event.Error.other", "Event.*"),
      ("Event.Error.other", "*.Event.*"),
      ("Event.error", "Event.Error"),
      ("EVENT.Error", "Event.Error")
    )

    forAll(notMatching) { (incoming: String, wanted: String) =>
      !builder.compareRoutingKeys(incoming, wanted) should be(true)
    }
  }

  def eventHandler(ev: Event): Future[String] = Future.successful(s"Event ${ev.eventId}")

  def messageHandler(msg: Message): Future[String] = Future.successful(s"Message ${msg.messageId}")
}
