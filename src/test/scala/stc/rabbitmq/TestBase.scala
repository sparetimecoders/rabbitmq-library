package stc.rabbitmq

import akka.Done
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.alpakka.amqp.scaladsl.CommittableIncomingMessage
import akka.stream.alpakka.amqp.{Declaration, IncomingMessage}
import akka.stream.scaladsl.Sink
import akka.testkit.TestKit
import akka.util.ByteString
import com.rabbitmq.client.AMQP.BasicProperties
import com.rabbitmq.client.Envelope
import org.json4s.ext.JZonedDateTimeSerializer
import org.json4s.jackson.Serialization.write
import org.json4s.{DefaultFormats, Formats}
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike}

import scala.collection.mutable.ListBuffer
import scala.collection.{Seq, immutable}
import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}
import scala.reflect.ClassTag

trait TestBase extends FlatSpecLike with BeforeAndAfterAll {
  implicit def system: ActorSystem
  implicit val formats: Formats = DefaultFormats + JZonedDateTimeSerializer
  implicit val ec: ExecutionContextExecutor = system.dispatcher
  implicit val mat: ActorMaterializer = ActorMaterializer()

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  def incomingMessage(msgId: String)(implicit ec: ExecutionContext): CheckMessage = {
    incomingMessage(Message(msgId, None), "routingkey")
  }
  def incomingMessage(msg: Message, routingKey: String)(implicit ec: ExecutionContext): CheckMessage = {
    incomingMessage(write(msg), routingKey)
  }
  def incomingMessage(json: String, routingKey: String)(implicit ec: ExecutionContext): CheckMessage = {

    new CheckMessage {
      val message = IncomingMessage(ByteString(json), new Envelope(1L, false, "exchange", routingKey), new BasicProperties())

      var acked: Boolean = false
      var nacked: Boolean = false

      override def ack(multiple: Boolean): Future[Done] = {
        acked = true
        Future(Done)
      }

      override def nack(multiple: Boolean, requeue: Boolean): Future[Done] = {
        nacked = true
        Future(Done)
      }
    }
  }
  def watchableSink[A](store: ListBuffer[A]): Sink[A, Future[immutable.Seq[A]]] = {
    Sink.seq[A].contramap(x => {
      store.append(x)
      x
    })
  }
  trait CheckMessage extends CommittableIncomingMessage {
    def acked: Boolean
    def nacked: Boolean
  }

  def findDeclaration[T](declarations: Seq[Declaration])(implicit classTag: ClassTag[T]): T =
    getDeclarationByType[T](declarations).head

  def getDeclarationByType[T](declarations: Seq[Declaration])(implicit classTag: ClassTag[T]): Seq[T] =
    declarations.groupBy(_.getClass.getSimpleName)(classTag.runtimeClass.getSimpleName).asInstanceOf[Seq[T]]

}

case class Message(messageId: String, data: Option[String])
case class Event(eventId: String, data: Option[String])
case class DelayedEvent(eventId: String, data: Option[String]) extends DelayedMessage {
  override def delay: Long = 10000
}
