package stc.rabbitmq

import akka.actor.{ActorSystem, CoordinatedShutdown}
import akka.stream.Materializer
import akka.stream.alpakka.amqp._
import akka.stream.alpakka.amqp.scaladsl.CommittableIncomingMessage
import akka.stream.scaladsl.{Flow, Sink, Source}
import akka.testkit.{TestKit, TestKitBase}
import akka.{Done, NotUsed}
import org.scalatest.Matchers
import org.scalatest.concurrent.PatienceConfiguration.Timeout
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Seconds, Span}

import scala.collection.immutable
import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.reflect.ClassTag
import scala.util.Try

class RabbitMqSpec extends TestKit(ActorSystem("RabbitMqSpec"))
  with Matchers
  with ScalaFutures
  with TestBase {

  override implicit val patienceConfig = PatienceConfig(Span(2000, Millis), Span(100, Millis))

  "RabbitMq" should "load reference config and setup values" in {
    val mq = new MyRabbitMq {
      bufferSize shouldBe 20
      eventExchangeDeclaration.name shouldBe "events.topic.exchange"
      eventExchangeDeclaration.autoDelete shouldBe false
      eventExchangeDeclaration.durable shouldBe true
      eventExchangeDeclaration.exchangeType shouldBe "x-delayed-message"
      eventExchangeDeclaration.arguments("x-delayed-type") shouldBe "topic"
      eventExchangeDeclaration.arguments should have size 1
      eventExchangeDeclaration.internal shouldBe false

      private val connectionDetails: AmqpDetailsConnectionProvider = connectionProvider.provider.asInstanceOf[AmqpDetailsConnectionProvider]
      connectionDetails.hostAndPortList should contain(("127.0.0.1", 5672))
    }
  }

  "outgoingFlow" should "convert to OutgoingMessage with parameters" in {
    val publishFlow = new MyRabbitMq {}
      .outgoingMessageFlow[Message](Some("key"))

    val result = Source.fromIterator(() => Seq("1").iterator)
      .map(x => Message(s"processed $x", None))
      .via(publishFlow).runWith(Sink.seq).futureValue

    result should have size 1
    result.head.routingKey shouldBe Some("key")
    result.head.immediate shouldBe false
    result.head.mandatory shouldBe false
    result.head.props.get.getDeliveryMode shouldBe 2
    result.head.props.get.getHeaders shouldBe empty
    result.head.bytes.utf8String shouldBe """{"messageId":"processed 1"}"""
  }

  "transientEventStreamListener" should "setup queues and exchanges" in {
    val mq: MyRabbitMq = new MyRabbitMq {
      override def createNamedAmqpQueueSource(queueName: String, declarations: Declaration*): Source[CommittableIncomingMessage, Any] = {
        this.declarations = declarations
        this.queueName = queueName
        Source.empty
      }

      override def createRestartingSource(sourceFactory: () => Source[CommittableIncomingMessage, Any]): Source[CommittableIncomingMessage, Any] = {
        sourceFactory.apply()
      }

      override def randomString: Host = "random"
    }
    mq.transientEventStreamListener(FlowBuilder(1)
      .add[Event, String]("key1", _ => Future("1"))
      .add[Message, Integer]("key2", _ => Future(1)))
    val expectedQueueName = "test-random"
    val expectedExchangeName = "events.topic.exchange"
    mq.queueName shouldBe expectedQueueName
    mq.declarations should have size 4
    val queueDeclarations = mq.declarations.filter(d => d.isInstanceOf[QueueDeclaration]).asInstanceOf[Seq[QueueDeclaration]]
    queueDeclarations should have size 1
    queueDeclarations.head.name shouldBe expectedQueueName
    queueDeclarations.head.arguments.get("x-expires") shouldBe None
    queueDeclarations.head.autoDelete shouldBe true
    queueDeclarations.head.durable shouldBe false
    queueDeclarations.head.exclusive shouldBe false

    val bindingDeclarations = mq.declarations.filter(d => d.isInstanceOf[BindingDeclaration]).asInstanceOf[Seq[BindingDeclaration]]
    bindingDeclarations should have size 2
    bindingDeclarations.map(_.routingKey.get) should contain theSameElementsAs Seq("key1", "key2")
    bindingDeclarations.forall(_.exchange == expectedExchangeName) shouldBe true
    bindingDeclarations.forall(_.queue == expectedQueueName) shouldBe true

    val exchangeDeclarations = mq.declarations.filter(d => d.isInstanceOf[ExchangeDeclaration]).asInstanceOf[Seq[ExchangeDeclaration]]
    exchangeDeclarations should have size 1
    exchangeDeclarations.head.name shouldBe expectedExchangeName
    exchangeDeclarations.head.internal shouldBe false
    exchangeDeclarations.head.autoDelete shouldBe false
    exchangeDeclarations.head.durable shouldBe true

    exchangeDeclarations.head.exchangeType shouldBe "x-delayed-message"
    exchangeDeclarations.head.arguments("x-delayed-type") shouldBe "topic"
    exchangeDeclarations.head.arguments should have size 1
  }

  "persistentEventStreamListener" should "setup queues and exchanges" in {
    val mq: MyRabbitMq = new MyRabbitMq {

      override def createNamedAmqpQueueSource(queueName: String, declarations: Declaration*): Source[CommittableIncomingMessage, Any] = {
        this.declarations = declarations
        this.queueName = queueName
        Source.empty
      }

      override def createRestartingSource(sourceFactory: () => Source[CommittableIncomingMessage, Any]): Source[CommittableIncomingMessage, Any] = {
        sourceFactory.apply()
      }
    }
    mq.persistentEventStreamListener(FlowBuilder(1)
      .add[Event, String]("key1", _ => Future("1"))
      .add[Message, Integer]("key2", _ => Future(1)))
    val expectedQueueName = "events.topic.exchange.queue.test"
    val expectedExchangeName = "events.topic.exchange"
    mq.queueName shouldBe expectedQueueName
    mq.declarations should have size 4
    val queueDeclarations = mq.declarations.filter(d => d.isInstanceOf[QueueDeclaration]).asInstanceOf[Seq[QueueDeclaration]]
    queueDeclarations should have size 1
    queueDeclarations.head.name shouldBe expectedQueueName
    queueDeclarations.head.arguments.get("x-expires") shouldBe Some(432000000)
    queueDeclarations.head.autoDelete shouldBe false
    queueDeclarations.head.durable shouldBe true
    queueDeclarations.head.exclusive shouldBe false

    val bindingDeclarations = mq.declarations.filter(d => d.isInstanceOf[BindingDeclaration]).asInstanceOf[Seq[BindingDeclaration]]
    bindingDeclarations should have size 2
    bindingDeclarations.map(_.routingKey.get) should contain theSameElementsAs Seq("key1", "key2")
    bindingDeclarations.forall(_.exchange == expectedExchangeName) shouldBe true
    bindingDeclarations.forall(_.queue == expectedQueueName) shouldBe true

    val exchangeDeclarations = mq.declarations.filter(d => d.isInstanceOf[ExchangeDeclaration]).asInstanceOf[Seq[ExchangeDeclaration]]
    exchangeDeclarations should have size 1
    exchangeDeclarations.head.name shouldBe expectedExchangeName
    exchangeDeclarations.head.internal shouldBe false
    exchangeDeclarations.head.autoDelete shouldBe false
    exchangeDeclarations.head.durable shouldBe true
    exchangeDeclarations.head.exchangeType shouldBe "x-delayed-message"
    exchangeDeclarations.head.arguments("x-delayed-type") shouldBe "topic"
    exchangeDeclarations.head.arguments should have size 1
  }

  it should "stop the flow if an NonResumable error occurs" in {
    implicit val system: ActorSystem = ActorSystem("should-shut-down")

    val promise = Promise[Done]
    val messages = List(incomingMessage("1"), incomingMessage("2"))
    new MyRabbitMq() {
      coordinatedShutdown.addTask(
        CoordinatedShutdown.PhaseBeforeServiceUnbind, "test"
      ) { () =>
          promise.complete(Try(Done))
          promise.future
        }

      override def createNamedAmqpQueueSource(queueName: String, declarations: Declaration*): Source[CommittableIncomingMessage, Any] = {
        Source(messages)
      }
    }.persistentEventStreamListener(FlowBuilder(1)
      .add[Message, String]("routingkey", _ => Future.failed(new IllegalStateException("FAILED"))))

    promise.future.futureValue(Timeout(Span(10, Seconds)))
    messages.head.nacked shouldBe true
    // Second message not processed
    messages.drop(1).head.nacked shouldBe false
  }

  it should "resume the flow if ResumableError occurs" in {
    val messages = List(incomingMessage("1"), incomingMessage("2"))
    new MyRabbitMq() {

      override def createNamedAmqpQueueSource(queueName: String, declarations: Declaration*): Source[CommittableIncomingMessage, Any] = {
        Source(messages)
      }
    }.persistentEventStreamListener(FlowBuilder(1)
      .add[Message, String]("routingkey", _ => Future.failed(new ResumableException("Yeah", new IllegalArgumentException("Ok")))))
    Thread.sleep(1000)
    // All messages processed
    messages.forall(_.acked) shouldBe true
  }

  "serviceListener" should "create stream flow" in {
    val serviceMq = new MyRabbitMq() {
      override def serviceName: String = "test-service"

      override def buildStreamListener(declarations: Seq[Declaration], flowBuilder: FlowBuilder, sink: Sink[CommittableIncomingMessage, Any])(implicit ec: ExecutionContext, materializer: Materializer): Unit = {
        declarations should have size 4
        findQueueNameFromDeclarations(declarations) shouldBe "test-service.direct.exchange.request.queue"
        val bindingDeclaration = findDeclaration[BindingDeclaration](declarations)
        bindingDeclaration.queue shouldBe "test-service.direct.exchange.request.queue"
        bindingDeclaration.exchange shouldBe "test-service.direct.exchange.request"
        val exchangeDeclarations = getDeclarationByType[ExchangeDeclaration](declarations)
        exchangeDeclarations.map(_.name) should contain only ("test-service.direct.exchange.request", "test-service.headers.exchange.response")
      }
    }
    serviceMq.serviceListener(FlowBuilder(1).add[String, Message]("key", s => Future(Message(s, None))))
  }

  "servicePublisher" should "setup exchange to receive OutgoingMessage" in {
    val complete = Promise[OutgoingMessage]()
    val clientMq = new MySinkRabbitMq(complete) {

      override def serviceName: String = "client-service"
    }
    val publisher = clientMq.servicePublisher[Message, Message]("test-service", m => m, "key")
    publisher ! Message("Peter", None)

    val result = complete.future.futureValue
    result.routingKey shouldBe Some("key")
    result.mandatory shouldBe false
    result.immediate shouldBe false
    result.props.get.getDeliveryMode shouldBe 2
    result.bytes.utf8String shouldBe """{"messageId":"Peter"}"""
    result.props.get.getHeaders.get("Host") shouldBe "client-service"
  }

  "finiteServicePublisher" should "send provided object to sink and terminate actor-system" in new TestKitBase {
    override implicit lazy val system: ActorSystem = ActorSystem("Terminating")

    val complete: Promise[OutgoingMessage] = Promise[OutgoingMessage]()
    val terminated: Promise[Done] = Promise[Done]()
    val clientMq: MySinkRabbitMq = new MySinkRabbitMq(complete) {

      override def serviceName: String = "client-service"
    }
    clientMq.coordinatedShutdown.addTask(
      CoordinatedShutdown.PhaseBeforeServiceUnbind, "test"
    ) { () =>
        terminated.success(Done).future
      }
    clientMq.finiteServicePublisher[Message]("test-service", Seq(Message("Peter", None)), "key")

    val result: OutgoingMessage = complete.future.futureValue
    result.routingKey shouldBe Some("key")
    result.mandatory shouldBe false
    result.immediate shouldBe false
    result.props.get.getDeliveryMode shouldBe 2
    result.bytes.utf8String shouldBe """{"messageId":"Peter"}"""
    result.props.get.getHeaders.get("Host") shouldBe "client-service"

    terminated.future.futureValue shouldBe Done
  }

  "eventStreamPublisher" should "send messages to event sink with proper values" in {
    val complete = Promise[OutgoingMessage]()
    val clientMq = new MySinkRabbitMq(complete) {}
    val publisher = clientMq.eventStreamPublisher[String, Event]((s: String) => Event(s, None))
    publisher ! "Peter"
    val result = complete.future.futureValue
    result.routingKey shouldBe None
    result.bytes.utf8String shouldBe """{"eventId":"Peter"}"""
    result.props.get.getHeaders shouldBe empty
    result.props.get.getDeliveryMode shouldBe 2
  }

  it should "send message with delay if instance of DelayedMessage" in {
    val complete = Promise[OutgoingMessage]()
    val clientMq = new MySinkRabbitMq(complete) {}
    val publisher = clientMq.eventStreamPublisher[String, DelayedEvent]((s: String) => DelayedEvent(s, None))
    publisher ! "Peter"
    val result = complete.future.futureValue
    result.routingKey shouldBe None
    result.bytes.utf8String shouldBe """{"eventId":"Peter"}"""
    result.props.get.getHeaders should have size 1
    result.props.get.getDeliveryMode shouldBe 2
  }

  "serviceResponseListener" should "setup resources and process message" in {
    val functionProcessed = Promise[String]
    val flowCompleted = Promise[String]
    val routingKey = "key"
    val msg = incomingMessage(Message("1", None), routingKey)
    new MyRabbitMq() {
      override def createNamedAmqpQueueSource(queueName: String, declarations: Declaration*): Source[CommittableIncomingMessage, Any] = {
        queueName shouldBe "a-new-cool-service.headers.exchange.response.queue.test"
        declarations should have size 3

        def getByType[T](implicit classTag: ClassTag[T]): Seq[T] =
          getDeclarationByType[T](declarations)

        val exchangeDeclarations = getByType[ExchangeDeclaration]
        val queueDeclarations = getByType[QueueDeclaration]
        val bindingDeclarations = getByType[BindingDeclaration]

        exchangeDeclarations.map(_.name) should contain only "a-new-cool-service.headers.exchange.response"
        queueDeclarations.map(_.name) should contain only "a-new-cool-service.headers.exchange.response.queue.test"
        bindingDeclarations.head.queue shouldBe "a-new-cool-service.headers.exchange.response.queue.test"
        bindingDeclarations.head.exchange shouldBe "a-new-cool-service.headers.exchange.response"
        bindingDeclarations.head.routingKey shouldBe Some(routingKey)
        Source.single(msg)
      }
    }.serviceResponseListener(
      "a-new-cool-service",
      FlowBuilder(1).add[Message, String](routingKey, msg => {
        functionProcessed.success("Result!")
        Future("Value")
      },
                                          Sink.seq.contramap(s => {
          flowCompleted.success(s)
          s
        }))
    )
    functionProcessed.future.futureValue shouldBe "Result!"
    flowCompleted.future.futureValue shouldBe "Value"

    msg.acked shouldBe true
    msg.nacked shouldBe false
  }

  "'resource names'" should "be generated correctly" in {
    val serviceMq = new MyRabbitMq() {
      override def serviceName: String = "parsers"
    }

    val clientMq = new MyRabbitMq() {
      override def serviceName: String = "property-service"
    }
    serviceMq.requestExchangeName("parsers") shouldBe "parsers.direct.exchange.request"
    serviceMq.requestQueueName("parsers") shouldBe "parsers.direct.exchange.request.queue"

    clientMq.responseExchangeName("parsers") shouldBe "parsers.headers.exchange.response"
    clientMq.responseQueueName("parsers") shouldBe "parsers.headers.exchange.response.queue.property-service"
  }
}

abstract class MyRabbitMq(implicit val system: ActorSystem) extends RabbitMq {

  var queueName: String = ""

  var declarations: Seq[Declaration] = Seq()

  override val coordinatedShutdown: CoordinatedShutdown = CoordinatedShutdown(system)

  override def serviceName: String = "test"
}

abstract class MySinkRabbitMq(promise: Promise[OutgoingMessage])(implicit val system: ActorSystem) extends RabbitMq {
  override val coordinatedShutdown: CoordinatedShutdown = CoordinatedShutdown(system)

  override def serviceName: String = "test"

  val sink: Sink[OutgoingMessage, Future[immutable.Seq[OutgoingMessage]]] =

    Sink.seq[OutgoingMessage].contramap(o => {
      promise.success(o)
      o
    })

  override private[rabbitmq] def createSink(exchangeDeclaration: ExchangeDeclaration): Sink[OutgoingMessage, Any] = {
    sink
  }
}
