package stc.rabbitmq

/**
 * Implement this trait to delay delivery of message
 */
trait DelayedMessage {
  /**
   *
   * @return the delay before delivering the message in milliseconds
   */
  def delay: Long
}
