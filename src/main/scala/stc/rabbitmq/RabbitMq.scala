package stc.rabbitmq

import java.net.InetAddress
import java.util.UUID

import akka.actor.CoordinatedShutdown.UnknownReason
import akka.actor.{Actor, ActorRef, ActorSystem, CoordinatedShutdown, Props, Status, Terminated}
import akka.stream._
import akka.stream.alpakka.amqp._
import akka.stream.alpakka.amqp.scaladsl.{AmqpSink, AmqpSource, CommittableIncomingMessage}
import akka.stream.scaladsl.{Flow, RestartSink, RestartSource, Sink, Source}
import akka.util.ByteString
import akka.{Done, NotUsed}
import com.rabbitmq.client._
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.StrictLogging
import org.json4s.Formats
import org.json4s.jackson.Serialization.write

import scala.collection.JavaConverters._
import scala.collection.Seq
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

class ResumableException(msg: String, t: Throwable) extends Exception(msg, t)

/**
 * Handles connections and general sending/receiving messages via RabbitMQ.
 * Messages sent on the queues *must* be JSON and *must* be mapped to a case class
 * (and a Format that is used to map between JSON and the case class).
 *
 * *Listener methods setup connections that will pass messages to functions based on the type
 * *Publisher methods retun actor refs that can be sent messages which will be forwarded to Rabbit
 *
 */
trait RabbitMq
  extends StrictLogging
  with PropsBuilder {

  implicit val system: ActorSystem
  private[rabbitmq] type Host = String
  private[rabbitmq] type Port = Int
  /**
   * The connection provider to use
   */
  private[rabbitmq] lazy val connectionProvider = {
    def rabbitMqLibraryVersion = Option(FlowBuilder.getClass.getPackage.getImplementationVersion).getOrElse("")

    def connectionName = s"$serviceName#$rabbitMqLibraryVersion#@${InetAddress.getLocalHost.getHostName}"

    AmqpCachedConnectionProvider(
      AmqpDetailsConnectionProvider(
        parseServersConfig(mqConfig.getString("servers").split(","))
      )
        .withVirtualHost(mqConfig.getString("vhost"))
        .withCredentials(AmqpCredentials(
          mqConfig.getString("user"),
          mqConfig.getString("password")
        ))
        .withAutomaticRecoveryEnabled(false)
        .withTopologyRecoveryEnabled(false)
        .withNetworkRecoveryInterval(10)
        .withRequestedHeartbeat(10)
        .withConnectionName(connectionName)
    )
  }
  val coordinatedShutdown: CoordinatedShutdown
  val decider: Supervision.Decider = {
    case recoverable: ResumableException =>
      logger.error("Recoverable exception caught", recoverable)
      Supervision.Resume

    case t: Throwable =>
      logger.error("Unrecoverable exception caught. Shutting down!", t)
      coordinatedShutdown.run(UnknownReason)
      Supervision.Stop
  }
  protected val bufferSize: Int = mqConfig.getInt("bufferSize")
  private[rabbitmq] val (min, max, random) = (1.seconds, 10.seconds, 0.2)
  private[rabbitmq] val deleteQueueAfter = 5.days
  private[rabbitmq] val queueExpirationProps: Map[String, AnyRef] = Map("x-expires" -> deleteQueueAfter.toMillis.asInstanceOf[AnyRef])

  /**
   * The name of the service that connects to rabbitmq.
   * Used to generate names for resources like queues and connections.
   *
   * @return the service name
   */
  def serviceName: String

  /**
   * Create a new persistent event stream listener.
   * Will create an event queue for the service bound to the event exchange (events.topic.exchange).
   *
   * @param flowBuilder the builder that will create the flow(s) to handle events on the event stream
   * @param sink        the sink to run with
   */
  def persistentEventStreamListener(
    flowBuilder: FlowBuilder,
    sink:        Sink[CommittableIncomingMessage, Any] = Sink.ignore
  )(implicit ec: ExecutionContext, materializer: Materializer): Unit = {
    val queueName = s"${eventExchangeDeclaration.name}.queue.$serviceName"

    val declarations = durableQueueDeclaration(queueName) :: (eventExchangeDeclaration :: flowBuilder.routingKeys.map(key => BindingDeclaration(queueName, eventExchangeDeclaration.name, Some(key))).toList)

    buildStreamListener(declarations, flowBuilder, sink)
    logger.info(s"Listening to events matched by routing keys: [${flowBuilder.routingKeys.mkString(",")}] for service $serviceName")
  }

  private[rabbitmq] def durableQueueDeclaration(name: String): QueueDeclaration =
    QueueDeclaration(
      name       = name,
      durable    = true,
      autoDelete = false,
      exclusive  = false,
      arguments  = queueExpirationProps
    )

  /**
   * Builds a listener for events and pass it to the defined flow
   *
   * @param declarations declarations to be creates (queue, exchange, binding)
   * @param flowBuilder  the flowbuilder to use to create a flow that handles stream events
   * @param sink         the sink to use
   */
  private[rabbitmq] def buildStreamListener(
    declarations: Seq[Declaration],
    flowBuilder:  FlowBuilder,
    sink:         Sink[CommittableIncomingMessage, Any] = Sink.ignore
  )(implicit ec: ExecutionContext, materializer: Materializer): Unit = {

    logger.debug(s"Creating a stream listener using declarations ${declarations.mkString(",")}")
    val killSwitch = KillSwitches.shared("kill-switch")

    val queueName = findQueueNameFromDeclarations(declarations)

    val source: Source[CommittableIncomingMessage, Any] = createNamedAmqpQueueSource(queueName, declarations: _*)
    createRestartingSource(() => source)

      .via(flowBuilder
        .build(killSwitch)
        .log("Error")
        .withAttributes(ActorAttributes.supervisionStrategy(decider)))
      .runWith(sink)

    coordinatedShutdown.addTask(
      CoordinatedShutdown.PhaseBeforeServiceUnbind, "closeStreams"
    ) { () =>
        killSwitch.shutdown()
        flowBuilder.completionPromise
      }
  }

  private[rabbitmq] def createNamedAmqpQueueSource(queueName: String, declarations: Declaration*): Source[CommittableIncomingMessage, Any] =
    AmqpSource.committableSource(
      NamedQueueSourceSettings(connectionProvider, queueName)
        .withDeclarations(declarations: _*),
      bufferSize = bufferSize
    )

  private[rabbitmq] def createRestartingSource(sourceFactory: () => Source[CommittableIncomingMessage, Any]): Source[CommittableIncomingMessage, Any] = {
    RestartSource.withBackoff(min, max, random) {
      sourceFactory
    }
  }

  private[rabbitmq] def findQueueNameFromDeclarations(declarations: Seq[Declaration]): String = declarations
    .find(d => d.isInstanceOf[QueueDeclaration])
    .getOrElse(new IllegalArgumentException("No queue declaration supplied"))
    .asInstanceOf[QueueDeclaration]
    .name

  /**
   * Create a new persistent service event stream listener.
   * Will create an event queue for the service bound to target service event exchange
   *
   * @param flowBuilder the builder that will create the flow(s) to handle events on the service event stream
   * @param sink        the sink to run with
   */
  def serviceListener(
    flowBuilder: FlowBuilder,
    sink:        Sink[CommittableIncomingMessage, Any] = Sink.ignore
  )(implicit ec: ExecutionContext, materializer: Materializer): Unit = {
    val reqQueueName = requestQueueName(serviceName)
    val requestExchangeDeclaration = directExchangeDeclaration(requestExchangeName(serviceName))
    val responseExchangeDeclaration = headersExchangeDeclaration(responseExchangeName(serviceName))

    val declarations = responseExchangeDeclaration :: durableQueueDeclaration(reqQueueName) :: (requestExchangeDeclaration
      :: flowBuilder.routingKeys.map(key => BindingDeclaration(reqQueueName, requestExchangeDeclaration.name, Some(key))).toList)

    buildStreamListener(declarations, flowBuilder, sink)
    logger.info(s"Listening to events matched by routing keys: [${flowBuilder.routingKeys.mkString(",")}] for service $serviceName")
  }

  private[rabbitmq] def requestQueueName(targetServiceName: String) = {
    s"${requestExchangeName(targetServiceName)}.queue"
  }

  /**
   * Create a new transient event stream listener.
   * Will create an event queue for the service bound to the event exchange (events.topic.exchange).
   *
   * @param flowBuilder the builder that will create the flow(s) to handle events on the event stream
   * @param sink        the sink to run with
   */
  def transientEventStreamListener(
    flowBuilder: FlowBuilder,
    sink:        Sink[CommittableIncomingMessage, Any] = Sink.ignore
  )(implicit ec: ExecutionContext, materializer: Materializer): Unit = {

    val queue = temporaryQueueDeclaration
    val declarations = queue :: (eventExchangeDeclaration :: flowBuilder.routingKeys.map(key => BindingDeclaration(queue.name, eventExchangeDeclaration.name, Some(key))).toList)

    buildStreamListener(declarations, flowBuilder, sink)
    logger.info(s"Listening to events matched by routing keys: [${flowBuilder.routingKeys.mkString(",")}] for service $serviceName")
  }

  private[rabbitmq] def temporaryQueueDeclaration: QueueDeclaration =
    QueueDeclaration(
      name       = s"$serviceName-$randomString",
      durable    = false,
      autoDelete = true,
      exclusive  = false
    )

  /**
   * Used to send message to a specific service (based on the service name)
   */
  def servicePublisher[T <: AnyRef](targetServiceName: String, flow: Flow[T, OutgoingMessage, NotUsed])(implicit ec: ExecutionContext, materializer: Materializer, formats: Formats, evidence: Manifest[T]): ActorRef = {
    logger.info(s"Created a publisher towards service $targetServiceName")
    streamPublisher(createSink(directExchangeDeclaration(requestExchangeName(targetServiceName))), flow)
  }

  def finiteServicePublisher[I <: AnyRef](targetServiceName: String, x: Seq[I], routingKey: String)(implicit ec: ExecutionContext, materializer: Materializer, formats: Formats, evidence: Manifest[I]): Unit = {
    val ref = servicePublisher[I, I](targetServiceName, (i: I) => i, routingKey)
    system.actorOf(Props(new Actor() {
      context.watch(ref)

      override def receive: Receive = {
        case Terminated(`ref`) =>
          coordinatedShutdown.run(UnknownReason)
      }
    }))
    x.foreach(ref ! _)
    // Hopefully streams are up and running before killing them again
    Thread.sleep(1000)
    ref ! Status.Success
  }

  /**
   * Used to send message to a specific service (based on the service name)
   *
   * @param fn         the function to be applied before sending the message
   * @param routingKey routingkey for message being sent
   * @return an ActorRef
   */
  def servicePublisher[I <: Any, T <: AnyRef](targetServiceName: String, fn: I => T, routingKey: String)(implicit ec: ExecutionContext, materializer: Materializer, formats: Formats, evidence: Manifest[T]): ActorRef = {
    logger.info(s"Created a publisher towards service $targetServiceName with routing key: $routingKey")
    streamPublisher(requestServiceSink(targetServiceName), outgoingFlow(fn, Some(routingKey), defaultResponseHeaders()))
  }

  /**
   *
   * @param targetServiceName name of the service
   * @return the service event Sink
   */
  def requestServiceSink(targetServiceName: String): Sink[OutgoingMessage, Any] =
    createSink(directExchangeDeclaration(requestExchangeName(targetServiceName)))

  private[rabbitmq] def requestExchangeName(targetServiceName: String) = {
    s"$targetServiceName.direct.exchange.request"
  }

  /**
   * Creates a Sink based on the exchange declaration
   *
   * @param exchangeDeclaration the exchange declaration
   * @return the Sink
   */
  private[rabbitmq] def createSink(exchangeDeclaration: ExchangeDeclaration): Sink[OutgoingMessage, Any] = {
    createRestartingSink(() => createExchangeSink(exchangeDeclaration))
  }

  private[rabbitmq] def createRestartingSink(sinkFactory: () => Sink[OutgoingMessage, Future[Done]]): Sink[OutgoingMessage, NotUsed] = {
    RestartSink.withBackoff(min, max, random) {
      sinkFactory
    }
  }

  /**
   * Creates an exchange sink based on the declaration
   *
   * @param exchangeDeclaration configuration for exchange
   * @return the Sink
   */
  private[rabbitmq] def createExchangeSink(exchangeDeclaration: ExchangeDeclaration): Sink[OutgoingMessage, Future[Done]] =
    AmqpSink(
      AmqpSinkSettings(connectionProvider).
        withExchange(exchangeDeclaration.name).
        withDeclarations(exchangeDeclaration)
    )

  /**
   * Creates a Flow from `I` to OutgoingMessage
   *
   * @param fn         the function that converts `I` to `T` (a case class which will be converted to JSON)
   * @param routingKey to set on the OutgoingMessage
   * @return the Flow
   */
  def outgoingFlow[I <: Any, T <: AnyRef](fn: I => T, routingKey: Option[String] = None, headers: Map[String, AnyRef] = Map())(implicit ec: ExecutionContext, materializer: Materializer, formats: Formats, evidence: Manifest[T]): Flow[I, OutgoingMessage, NotUsed] = {
    Flow[I].map(fn).via(outgoingMessageFlow(routingKey, headers))
  }

  /**
   * Flow that will convert `T` case classes into JSON strings and wrap them into OutgoingMessages
   *
   * @param routingKey the routing key for the OutgoingMessage
   * @tparam T type of the message being converted to JSON
   * @return the actual Flow
   */
  private[rabbitmq] def outgoingMessageFlow[T <: AnyRef](routingKey: Option[String] = None, headers: Map[String, AnyRef] = Map())(implicit ec: ExecutionContext, materializer: Materializer, formats: Formats, evidence: Manifest[T]): Flow[T, OutgoingMessage, NotUsed] = Flow[T]
    .log("Error")
    .map[OutgoingMessage](
      s => {
        val props = buildAmqpProps(headers, s)
        OutgoingMessage(ByteString(write(s)), immediate = false, mandatory = false, props, routingKey)
      }
    )

  private[rabbitmq] def defaultResponseHeaders(): Map[String, AnyRef] = Map(responseHeader)

  private[rabbitmq] def responseHeader: (Host, String) = "Host" -> serviceName

  /**
   * Creates a publisher (Actor) that can be passed messages and output them to the stream via flow
   *
   * @param stream the stream to publish to
   * @param flow   the flow
   * @return
   */
  private[rabbitmq] def streamPublisher[I <: Any, T <: AnyRef](stream: Sink[OutgoingMessage, Any], flow: Flow[I, OutgoingMessage, NotUsed])(implicit ec: ExecutionContext, materializer: Materializer, formats: Formats, evidence: Manifest[T]): ActorRef = {
    stream.
      runWith(
        Source
          .actorRef(1000, OverflowStrategy.dropTail)
          .via(
            flow
          )
      )
  }

  /**
   * Creates a publisher of events to the event exchange (events.topic.exchange).
   *
   * @param fn         the function to be applied to each message
   * @param routingKey the routingKey for the OutgoingMessage on the event exchange
   * @return an ActorRef
   */
  def eventStreamPublisher[I <: Any, T <: AnyRef](fn: I => T, routingKey: Option[String] = None)(implicit ec: ExecutionContext, materializer: Materializer, formats: Formats, evidence: Manifest[T]): ActorRef = {
    logger.info(s"Created a publisher to ${eventExchangeDeclaration.name}, with routing key $routingKey")
    eventStreamPublisher(outgoingFlow(fn, routingKey))
  }

  /**
   * Creates a publisher of events to the event exchange (events.topic.exchange).
   */
  def eventStreamPublisher[I <: Any, T <: AnyRef](flow: Flow[I, OutgoingMessage, NotUsed])(implicit ec: ExecutionContext, materializer: Materializer, formats: Formats, evidence: Manifest[T]): ActorRef = {
    logger.info(s"Created a publisher to ${eventExchangeDeclaration.name}")
    streamPublisher(createEventSink(), flow)
  }

  /**
   * @return the event exchange Sink
   */
  def createEventSink(): Sink[OutgoingMessage, Any] = {
    createSink(eventExchangeDeclaration)
  }

  /**
   *
   * @param targetServiceName the service to listen for responses from
   * @param flowBuilder       the flowbuilder specifying what to do with the response messages
   */
  def serviceResponseListener(
    targetServiceName: String,
    flowBuilder:       FlowBuilder,
    sink:              Sink[CommittableIncomingMessage, Any] = Sink.ignore
  )(implicit ec: ExecutionContext, materializer: Materializer): Unit = {
    val queueName = responseQueueName(targetServiceName)
    val exchangeDeclaration = headersExchangeDeclaration(responseExchangeName(targetServiceName))

    val declarations = durableQueueDeclaration(queueName) :: (exchangeDeclaration
      :: flowBuilder.routingKeys.map(key => BindingDeclaration(queueName, exchangeDeclaration.name, Some(key), defaultResponseHeaders() + ("x-match" -> "all"))).toList)

    buildStreamListener(declarations, flowBuilder, sink)
    logger.info(s"Listening to events matched by routing keys: [${flowBuilder.routingKeys.mkString(",")}] from service $targetServiceName")
  }

  private[rabbitmq] def responseQueueName(targetServiceName: String) = {
    s"${responseExchangeName(targetServiceName)}.queue.$serviceName"
  }

  private[rabbitmq] def responseExchangeName(targetServiceName: String) = {
    s"$targetServiceName.headers.exchange.response"
  }

  /**
   *
   * @param name the name of the exchange to create
   * @return the declaration
   */
  private[rabbitmq] def headersExchangeDeclaration(name: String): ExchangeDeclaration =
    exchangeDeclaration(
      name,
      "headers",
      durable = true
    )

  /**
   * Event exchange declaratiomn
   */
  private[rabbitmq] val eventExchangeDeclaration: ExchangeDeclaration =
    exchangeDeclaration(
      mqConfig.getString("eventExchangeName"),
      "topic",
      durable = true
    )

  /**
   *
   * @param name the name of the exchange to create
   * @return the declaration
   */
  private[rabbitmq] def directExchangeDeclaration(name: String): ExchangeDeclaration =
    exchangeDeclaration(
      name,
      "direct",
      durable = true
    )

  private[rabbitmq] def exchangeDeclaration(name: String, `type`: String, durable: Boolean): ExchangeDeclaration = {
    val args = Map("x-delayed-type" -> `type`)
    ExchangeDeclaration(
      name         = name,
      exchangeType = "x-delayed-message",
      durable      = durable,
      arguments    = args
    )
  }
  /**
   * Create a new persistent service event stream listener.
   * Will create an event queue for the service bound to target service event exchange
   *
   * @param flowBuilder the builder that will create the flow(s) to handle events on the service event stream
   * @param sink        the sink to run with
   */
  def serviceRequestHandler(
    flowBuilder: FlowBuilder,
    sink:        Sink[CommittableIncomingMessage, Any] = Sink.ignore
  )(implicit ec: ExecutionContext, materializer: Materializer): Unit = {
    val queueName = requestQueueName(serviceName)
    val exchangeDeclaration = directExchangeDeclaration(requestExchangeName(serviceName))

    val declarations = durableQueueDeclaration(queueName) :: (exchangeDeclaration
      :: flowBuilder.routingKeys.map(key => BindingDeclaration(queueName, exchangeDeclaration.name, Some(key))).toList)

    buildStreamListener(declarations, flowBuilder, sink)
    logger.info(s"Listening to events matched by routing keys: [${flowBuilder.routingKeys.mkString(",")}] for service $serviceName")
  }

  /**
   * @return the event exchange Sink
   */
  def createNonRestartingEventSink(): Sink[OutgoingMessage, Future[Done]] = {
    createExchangeSink(eventExchangeDeclaration)
  }

  def responseFlow[T <: AnyRef](targetServiceName: String, routingKey: Option[String] = None, headers: Map[String, AnyRef] = Map())(implicit ec: ExecutionContext, materializer: Materializer, formats: Formats, evidence: Manifest[T]): Sink[T, NotUsed] = {
    outgoingMessageFlow(routingKey, headers).to(responseServiceSink(targetServiceName))
  }

  /**
   *
   * @param targetServiceName name of the service
   * @return the service event Sink
   */
  def responseServiceSink(targetServiceName: String): Sink[OutgoingMessage, Any] =
    createSink(headersExchangeDeclaration(responseExchangeName(targetServiceName)))

  def requestFlow[T <: AnyRef](targetServiceName: String, routingKey: String)(implicit ec: ExecutionContext, materializer: Materializer, formats: Formats, evidence: Manifest[T]): Sink[T, NotUsed] = {
    outgoingMessageFlow(Some(routingKey), defaultResponseHeaders()).to(requestServiceSink(targetServiceName))
  }

  private[rabbitmq] def mqConfig = ConfigFactory.load().getConfig("mq")

  /**
   * Parse servers configuration
   *
   * @param servers the list of servers as <host>:<port>
   * @return the AMQP config (host:port)
   */
  private[rabbitmq] def parseServersConfig(servers: Seq[String]): scala.collection.immutable.Seq[(Host, Port)] = {
    servers
      .map { server =>
        server.split(":", 2) match {
          case Array(h: String, p: String) =>
            (h, p.toInt)
          case Array(h: String) =>
            (h, 5672)
          case _ =>
            throw new IllegalArgumentException(s"MQ Server configuration is invalid. Could not parse $server to a valid host:port configuration")
        }
      }.to[scala.collection.immutable.Seq]
  }

  private[rabbitmq] def randomString = {
    UUID.randomUUID().toString
  }
}

trait PropsBuilder {
  private[rabbitmq] def buildAmqpProps(headers: Map[String, AnyRef], message: AnyRef): Option[AMQP.BasicProperties] = {
    val processedHeaders = message match {
      case message1: DelayedMessage =>
        val x = "x-delay" -> message1.delay.asInstanceOf[AnyRef]
        headers + x
      case _ => headers
    }
    Some(new AMQP.BasicProperties.Builder().deliveryMode(2).headers(processedHeaders.asJava).build())
  }
}
