package stc.rabbitmq

import akka.stream.alpakka.amqp.OutgoingMessage
import akka.stream.alpakka.amqp.scaladsl.CommittableIncomingMessage
import akka.stream.scaladsl.{Flow, Sink}
import akka.stream.{Materializer, SharedKillSwitch}
import akka.util.ByteString
import akka.{Done, NotUsed}
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.StrictLogging
import org.json4s.Formats
import org.json4s.jackson.Serialization.{read, write}

import scala.collection.JavaConverters._
import scala.collection.Seq
import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.util.{Failure, Success, Try}

/**
 * FlowBuilder builds a Flow from functions and routing keys
 */
trait FlowBuilder extends PropsBuilder {
  /**
   *
   * @return the promise that will be completed when all flows have completed
   */
  def completionPromise()(implicit ec: ExecutionContext): Future[Done]

  /**
   * Add a new function mapping to the flow.
   *
   * @param routingKey events matching this routingkey will be forwarded to the function fn
   * @param fn         the function which will process the message
   * @param sink       the (optional) sink which to use for this subflow
   * @tparam I the type into which the CommittableIncomingMessage will be parsed into
   * @tparam O the type of the result from the function fn
   * @param ec       execution context
   * @param formats  needed to convert to I
   * @param evidence needed to convert to I
   * @return the updated FlowBuilder instances
   */
  def add[I <: Any, O <: Any](routingKey: String, fn: I => Future[O], sink: Sink[O, Any] = Sink.ignore)(implicit ec: ExecutionContext, formats: Formats, evidence: Manifest[I]): FlowBuilder

  def addRequestResponse[I <: Any, O <: AnyRef](routingKey: String, fn: I => Future[O], sink: Sink[OutgoingMessage, Any] = Sink.ignore)(implicit ec: ExecutionContext, formats: Formats, evidence: Manifest[I]): FlowBuilder

  /**
   * Constructs the actual flow
   */
  private[rabbitmq] def build(killSwitch: SharedKillSwitch)(implicit ec: ExecutionContext): Flow[CommittableIncomingMessage, CommittableIncomingMessage, Any]

  /**
   * The routing keys associated with a functiom/flow of this builder
   */
  private[rabbitmq] def routingKeys: Seq[String]
}

object FlowBuilder extends StrictLogging {
  protected val defaultWorkerCount: Int = ConfigFactory.load.getInt("mq.workerCount")

  case class State(sink: Sink[CommittableIncomingMessage, Any], promise: Promise[Done])

  def apply(): FlowBuilder = new DefaultFlowBuilder(defaultWorkerCount)

  def apply(workerCount: Int): FlowBuilder = new DefaultFlowBuilder(workerCount)

  def apply(workerCount: Int, killSwitch: SharedKillSwitch): FlowBuilder = new DefaultFlowBuilder(workerCount)

  private[rabbitmq] class DefaultFlowBuilder(workerCount: Int, state: Map[String, State] = Map()) extends FlowBuilder {
    def process[I, O](msg: CommittableIncomingMessage, fn: I => Future[O])(implicit ec: ExecutionContext, formats: Formats, evidence: Manifest[I]): Future[Option[O]] = {

      val cc = Try {
        read[I](msg.message.bytes.utf8String)
      }

      cc.fold(
        fa => {
          logger.error(s"Failed to parse json into a valid Event from event stream, will ACK message anyway. ${messageString(msg)}", fa)
          msg.ack(false)
          Future(None)
        },
        fb => {
          val result = fn(fb).map(x => Option(x))
          result.onComplete {
            case Success(_) =>
              msg.ack(false)

            case Failure(t) =>
              t match {
                case _: ResumableException =>
                  logger.error(s"Failed to process event with recoverable error, will ACK message. ${messageString(msg)}", t)
                  msg.ack(false)
                case _ =>
                  logger.error(s"Failed to process event, will NACK message. ${messageString(msg)}", t)
                  msg.nack(multiple = false)
              }
          }
          result
        }
      )
    }

    override def add[I <: Any, O <: Any](routingKey: String, fn: I => Future[O], sink: Sink[O, Any])(implicit ec: ExecutionContext, formats: Formats, evidence: Manifest[I]): FlowBuilder = {
      checkValidAddition(routingKey, fn)

      val flowPromise = Promise[Done]

      val flow = Flow[CommittableIncomingMessage]
        .watchTermination() { (_, done) =>
          done.onComplete(_ => {
            flowPromise.complete(Try(Done))
          })
        }
        .mapAsyncUnordered(workerCount)(msg => process(msg, fn))
        .filter(_.isDefined)
        .map(_.get)
        .to(sink)

      new DefaultFlowBuilder(workerCount, state.updated(routingKey, State(flow, flowPromise)))
    }

    def addRequestResponse[I <: Any, O <: AnyRef](routingKey: String, fn: I => Future[O], sink: Sink[OutgoingMessage, Any])(implicit ec: ExecutionContext, formats: Formats, evidence: Manifest[I]): FlowBuilder = {
      checkValidAddition(routingKey, fn)
      val flowPromise = Promise[Done]

      val flow = Flow[CommittableIncomingMessage]
        .watchTermination() { (_, done) =>
          done.onComplete(_ => {
            flowPromise.complete(Try(Done))
          })
        }.mapAsyncUnordered(workerCount)(msg => {

          process(msg, fn).filter(_.isDefined).map(_.get).map(s => {
            val props = buildAmqpProps(msg.message.properties.getHeaders.asScala.toMap, s)
            OutgoingMessage(ByteString(write(s)), immediate = false, mandatory = false, props, Some(routingKey))
          })
        })
        .to(sink)

      new DefaultFlowBuilder(workerCount, state.updated(routingKey, State(flow, flowPromise)))
    }

    override def build(killSwitch: SharedKillSwitch)(implicit ec: ExecutionContext): Flow[CommittableIncomingMessage, CommittableIncomingMessage, Any] = {
      state.foldLeft(Flow[CommittableIncomingMessage].via(killSwitch.flow)) { (acc, state) =>
        acc.divertTo(state._2.sink, msg => compareRoutingKeys(msg.message.envelope.getRoutingKey, state._1))
      }.via(unhandledMessagesFlow())
    }

    private[rabbitmq] def compareRoutingKeys(incomingRoutingKey: String, wantedRoutingKey: String): Boolean = {
      val pattern = wantedRoutingKey
        .replace("*", "[A-Za-z]+")
        .replace(".", "\\.")
        .replace("#", ".*[A-Za-z].*")
      incomingRoutingKey.matches(pattern)
    }

    override def routingKeys: Seq[String] = state.keySet.toSeq

    private def checkValidAddition[I <: Any, O <: Any](routingKey: String, fn: I => Future[O]): Unit = {
      state.get(routingKey).map(_ => throw new IllegalStateException(s"Routingkey [$routingKey] already exists in Flowbuilder."))
      if (fn == null) throw new IllegalStateException(s"Trying to add a null function for routingkey [$routingKey]")
    }

    private def unhandledMessagesFlow()(implicit ec: ExecutionContext): Flow[CommittableIncomingMessage, CommittableIncomingMessage, NotUsed] = {
      Flow[CommittableIncomingMessage]
        .mapAsyncUnordered(workerCount)(msg => {
          logger.warn(s"Unhandled message, acknowledging it. ${messageString(msg)}")
          msg.ack(false)
          Future(msg)
        })
    }

    /**
     * Flow that will convert `T` case classes into JSON strings and wrap them into OutgoingMessages
     *
     * @param routingKey the routing key for the OutgoingMessage
     * @tparam T type of the message being converted to JSON
     * @return the actual Flow
     */
    private[rabbitmq] def outgoingMessageFlow[T <: AnyRef](routingKey: Option[String] = None, headers: Map[String, AnyRef] = Map())(implicit ec: ExecutionContext, materializer: Materializer, formats: Formats, evidence: Manifest[T]): Flow[T, OutgoingMessage, NotUsed] = {

      Flow[T]
        .log("Error")
        .map[OutgoingMessage](
          s => {
            val props = buildAmqpProps(headers, s)
            OutgoingMessage(ByteString(write(s)), immediate = false, mandatory = false, props, routingKey)
          }
        )
    }

    private def messageString(msg: CommittableIncomingMessage): String = s"Envelope: ${msg.message.envelope}, message: ${msg.message.bytes.utf8String}"

    /**
     *
     * @return the promise that will be completed when all flows have completed
     */
    override def completionPromise()(implicit ec: ExecutionContext): Future[Done] = {
      Future.sequence(state.map(_._2.promise.future)).map(_ => Done)
    }
  }
}

